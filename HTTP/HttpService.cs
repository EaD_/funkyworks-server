﻿/*
    File: HttpService.cs
    Author: Elio Deçolli.
    Purpose: Base HTTP layer class.
    Last Updated: Unknown.
*/

using System;
using System.Net;
using System.Threading;

namespace TeamWork_Server.HTTP
{
    public class HttpContextReceivedEventArgs : EventArgs
    {
        public HttpListenerContext Context;

        public HttpContextReceivedEventArgs(HttpListenerContext c)
        {
            Context = c;
        }
    }

    public class HttpService
    {
        HttpListener listener;

        public HttpService(string ip, int port)
        {
            listener = new HttpListener();
            listener.Prefixes.Add("http://" + ip + ":" + port.ToString() + "/");
        }

        public event EventHandler<HttpContextReceivedEventArgs> HttpContextReceived;

        private void Gotcha(IAsyncResult ar)
        {
            HttpListenerContext context = listener.EndGetContext(ar);
            ThreadPool.QueueUserWorkItem(delegate
            {
                if (HttpContextReceived != null)
                {
                    HttpContextReceived(this, new HttpContextReceivedEventArgs(context));
                }
            });
            listener.BeginGetContext(Gotcha, null);
        }

        public void Start()
        {
            listener.Start();
            listener.BeginGetContext(Gotcha, null);
        }

        public void Pause()
        {
            listener.Stop();
        }
    }
}
