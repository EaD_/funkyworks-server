﻿/*
    File: CodeUpdateService.cs
    Author: Elio Deçolli.
    Purpose:  Allows users to submit their work.
    Last Updated: Unknown.

    Note: This ONLY sends the updates to the users!
*/

using System.Collections.Generic;
using System.Linq;
using System.IO;
using TeamWork_Server.HTTP;

namespace TeamWork_Server
{

    public class CodeUpdateTemplate
    {
        public string Author;
        public long Date;
        public string File;
        public string Code;
    }

    public class CodeUpdateService
    {
        public CodeUpdateService()
        {
        }

        private List<CodeUpdateTemplate> CodeUpdates = new List<CodeUpdateTemplate>();

        public void service_HttpContextReceived(object sender, HttpContextReceivedEventArgs e)
        {
            if (e.Context.Request.Headers["packet_type"] == "CodeReview")
            {
                int id = int.Parse(e.Context.Request.Headers["UID"]);
                BinaryWriter writer = new BinaryWriter(e.Context.Response.OutputStream);
                if (!CredentialManager.HasPermission(Credentials.CanDownload, MongoUtils.MongoUtils.FindUserByID(id).Role))
                {
                    writer.Write(false);
                    writer.Write("User has not sufficient permissions to access this feature.");
                }
                else
                {
                    string fName = e.Context.Request.Headers["file_name"];
                    Log.Info("Sending modification entries for file " + fName);
                    var updates = MongoUtils.MongoUtils.FindUpdatesForFile(fName);
                    writer.Write(true);
                    writer.Write(updates.Count());
                    foreach (var v in updates)
                    {
                        writer.Write(v.Author);
                        writer.Write(v.Date);
                        writer.Write(v.Details);
                        writer.Write(v.Blob);
                    }
                }
                writer.Flush();
                e.Context.Response.Close();

            }
        }
    }
}
