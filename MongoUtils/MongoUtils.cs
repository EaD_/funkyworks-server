﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;

namespace TeamWork_Server.MongoUtils
{
    public class User
    {
        [BsonId]
        private int _id;
        [BsonElement]
        public int ID;
        [BsonElement]
        public string Name;
        [BsonElement]
        public string Password;
        [BsonElement]
        public string Role;
    }

    public class TWTask
    {
        [BsonId]
        private int _id;
        [BsonElement]
        public int TaskID;
        [BsonElement]
        public string TaskName;
        [BsonElement]
        public string TaskDescription;
        [BsonElement]
        public string[] UserSpecified;
        [BsonElement]
        public long DueDate;
        [BsonElement]
        public int PercentDone;
        [BsonExtraElements]
        public TWTask ParentTask;
    }

    public class CodeUpdate
    {
        [BsonId]
        private int _id;
        [BsonElement]
        public string Author;
        [BsonElement]
        public long Date;
        [BsonElement]
        public string FileName;
        [BsonElement]
        public string Blob;
        [BsonElement]
        public string Details;
    }


    public class MongoUtils
    {
        private static string ConnectionString = "mongodb://localhost";
        private static MongoClient Client;
        

        public static void StartConnection()
        {
            
            Client = new MongoClient(ConnectionString);
            /*Client.GetDatabase("main").CreateCollection("users");
            Client.GetDatabase("main").CreateCollection("tasks");*/
            Log.Debug("MONGO DB: Connection has been established.");
        }

        public static User[] FindUsersByName(string name)
        {
            IMongoCollection<User> col = Client.GetDatabase("main").GetCollection<User>("users");

            var retval = col.AsQueryable().Where(p => p.Name.ToLower() == name.ToLower());
            return retval.ToArray();
        }

        public static User FindUserByID(int id)
        {
            IMongoCollection<User> col = Client.GetDatabase("main").GetCollection<User>("users");

            var retval = col.AsQueryable().Where(p => p.ID == id);
            if (retval.Count() > 0)
                return retval.First();
            else
                return null;
        }

        public static string[] GetUserNames()
        {
            IMongoCollection<User> col = Client.GetDatabase("main").GetCollection<User>("users");

            List<string> retvals = new List<string>();
            foreach (var u in col.AsQueryable())
                retvals.Add(u.Name);
            return retvals.ToArray();
        }

        public static TWTask[] FindTasksForUserID(int id)
        {
            User u = FindUserByID(id);
            if(u != null)
            {
                IMongoCollection<TWTask> col = Client.GetDatabase("main").GetCollection<TWTask>("tasks");

                var retval = col.AsQueryable().Where(p => p.UserSpecified.ToList().Contains(u.Name));
                return retval.ToArray();
            }
            return null;
        }

        public static User CreateUser(string name, string password, string role)
        {
            User u = new User();
            u.Name = name;
            u.Password = password;
            u.Role = role;
            u.ID = new Random().Next();

            Client.GetDatabase("main").GetCollection<User>("users").InsertOneAsync(u);
            return u;
        }

        public static void ModifyUserDetails(int id, User details)
        {
            IMongoCollection<User> col = Client.GetDatabase("main").GetCollection<User>("users");
            var builder = Builders<User>.Filter;
            var filter = builder.Eq("ID", id);

            var update = Builders<User>.Update
                .Set("Name", details.Name).Set("Password", details.Password).Set("Role", details.Role);

            col.UpdateOneAsync(filter, update);
        }

        public static void AddCodeUpdate(CodeUpdate blob)
        {
            Client.GetDatabase("main").GetCollection<CodeUpdate>("code_update").InsertOneAsync(blob);
            Log.Info(string.Format("New code update for file {0} has been submited by {1}.", new object[] { blob.FileName, blob.Author }));
        }

        public static CodeUpdate[] FindUpdatesByUsername(string name)
        {
            var col = Client.GetDatabase("main").GetCollection<CodeUpdate>("code_update");
            var retval = col.AsQueryable().Where(p => p.Author == name);

            return retval.ToArray();
        }

        public static CodeUpdate[] FindUpdatesForFile(string fileName)
        {
            var col = Client.GetDatabase("main").GetCollection<CodeUpdate>("code_update");
            var retval = col.AsQueryable().Where(p => p.FileName == fileName);

            return retval.ToArray();
        }

        public static void CreateTask(TWTask task)
        {
            var col = Client.GetDatabase("main").GetCollection<TWTask>("tasks");
            col.InsertOneAsync(task);
            Log.Info(string.Format("A new task \"{0}\" has been added.", task.TaskName));
        }

        public static void ModifyTask(int id, TWTask details)
        {
            var col = Client.GetDatabase("main").GetCollection<TWTask>("tasks");
            var builder = Builders<TWTask>.Filter;
            var filter = builder.Eq("TaskID", id);

            var update = Builders<TWTask>.Update
                .Set("TaskName", details.TaskName).Set("TaskDescription", details.TaskDescription).Set("PercentDone", details.PercentDone)
                .Set("UserSpecified", details.UserSpecified).Set("DueDate", details.DueDate);
            col.UpdateOneAsync(filter, update);
        }

        public static TWTask[] FindWIPTasks()
        {
            var col = Client.GetDatabase("main").GetCollection<TWTask>("tasks");
            var retvals = col.AsQueryable().Where(p => p.PercentDone < 100);

            return retvals.ToArray();
        }

        public static bool IsTaskDone(int id)
        {
            var col = Client.GetDatabase("main").GetCollection<TWTask>("tasks");
            var retvals = col.AsQueryable().Where(p => p.TaskID == id);

            var lst = retvals.ToList();
            if (lst.Count > 0)
                return lst[0].PercentDone == 100;
            return false;
        }

        public static TWTask[] FindTasksAssignedForUser(string name)
        {
            var col = Client.GetDatabase("main").GetCollection<TWTask>("tasks");
            var retvals = col.AsQueryable().Where(p => p.UserSpecified.ToList().Contains(name));

            return retvals.ToArray();
        }
    }
}
