﻿/*
    File: RedirectService.cs
    Author: Elio Deçolli.
    Purpose: Analyzes each packet and chooses what to do with the data. This elminates the need to have multiple ports binded.
    Last Updated: Unknown.


    Side Notes: As of now, Heartbeats are not available anymore, as we won't check if the user is online or not. This will be updated later, I think we should focus on the main things first.
*/

using System.Collections.Generic;
using System.IO;
using System.Net;
using TeamWork_Server.HTTP;
using System.Threading;
using TeamWork_Server.MongoUtils;

namespace TeamWork_Server.Core
{
    public class RedirectService
    {
        HttpService service;
        FileTransfer fService;
        TWServerConfiguration cfg;

        static public int HB_TIMEOUT = 0x5;

        public RedirectService(string host, TWServerConfiguration config)
        {
            service = new HttpService(host, 55987);
            cfg = config;
            service.HttpContextReceived += service_HttpContextReceived;
            fService = new FileTransfer(config);
            service.HttpContextReceived += fService.service_HttpContextReceived;
            service.HttpContextReceived += fService.cus.service_HttpContextReceived;
        }

        private List<CodeUpdateTemplate> CodeUpdates = new List<CodeUpdateTemplate>();

        public void Start()
        {
            service.Start();
            Log.Debug("Redirection Service is now available!");
        }
        
        private void service_HttpContextReceived(object sender, HttpContextReceivedEventArgs e)
        {
            HttpListenerRequest req = e.Context.Request;

            switch(req.Headers["TYPE"])
            {
                case "HeartBeat":
                    int id = int.Parse(req.Headers["UID"]);
                    //User.Users[id].Beat();
                break;

                case "Notification":
               
                break;

                case "NewUser":
                ThreadPool.QueueUserWorkItem(delegate
                {
                    string[] user_credentials = req.Headers["uCred"].Split('|');
                    User u = MongoUtils.MongoUtils.CreateUser(user_credentials[0], user_credentials[1], user_credentials[2]);
                    MemoryStream memsr = new MemoryStream();
                    BinaryWriter writer = new BinaryWriter(memsr);
                    writer.Write(u.Name);
                    writer.Write(u.Password);
                    writer.Write(u.Role);
                    writer.Write(u.ID);
                    writer.Flush();

                    byte[] bts = memsr.ToArray();
                    e.Context.Response.OutputStream.Write(bts, 0, bts.Length);
                    Log.Info(string.Format("New user {Name: {0} Role: {1} ID: {2}} has been craeted.", new object[] { u.Name, u.Role, u.ID }));
                    e.Context.Response.Close();
                });
                break;

                case "Login":
                string[] user_details = req.Headers["uCred"].Split('|');
                Log.Info("Trying to login user " + user_details[0]);
                    User[] vs = MongoUtils.MongoUtils.FindUsersByName(user_details[0]);
                    if(vs.Length > 0) {
                        User v = vs[0];
                        if (v.Name == user_details[0])
                        {
                            if (v.Password == user_details[1])
                            {
                                e.Context.Response.AddHeader("LG_RESULT", "OK");
                            }
                            else
                                e.Context.Response.AddHeader("LG_RESULT", "The specified password was invalid.");
                        }
                        goto BREAK;
                    }
                e.Context.Response.AddHeader("LG_RESULT", "Username could not be found. Please register before proceeding.");
            BREAK:
                e.Context.Response.Close();
                break;

                case "ParseNotifications":  // We store notifications as binary-encoded data on a file then parse only the latest ones (3 days old max). This is nasty but works so I don't really care for this at the moment /tehe
                
                 break;

                case "GetInfo":
                    BinaryWriter writer221 = new BinaryWriter(e.Context.Response.OutputStream);
                    writer221.Write(cfg.ProjectName);
                    writer221.Write(cfg.Description);
                    writer221.Write(cfg.Language);
                    writer221.Write(cfg.Version);
                    writer221.Flush();
                    e.Context.Response.Close();
                    break;
            }
        }
    }
}
