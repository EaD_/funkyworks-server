﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace TeamWork_Server.Core
{
    public class RSATools
    {
        private static RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

        public static void GenerateKeys()
        {
            File.WriteAllText("rsa_private.xml", rsa.ToXmlString(true));
            File.WriteAllText("rsa_public.xml", rsa.ToXmlString(false));

            Log.Debug("RSA Key has been generated successfuly.");
        }

        public static byte[] DecryptData(byte[] blob)
        {
            return rsa.Decrypt(blob, true);
        }
    }
}
